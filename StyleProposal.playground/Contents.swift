//: Playground - noun: a place where people can play

import UIKit

// From code

// Option 1: Free functions
func apply<T>(style: Style<T>, to: T) where T: UIView {
    style.apply(to: to)
}


// Option 2: Extensions
extension UIView {
    func apply(style: Style<UIView>) {
        style.apply(to: self)
    }
    
    func apply(styles: Style<UIView>...) {
        styles.forEach { style in
            style.apply(to: self)
        }
    }
}

extension UILabel {
    func apply(style: Style<UILabel>) {
        style.apply(to: self)
    }
    
    func apply(styles: Style<UILabel>...) {
        styles.forEach { style in
            style.apply(to: self)
        }
    }
}

// Option 3. call methods on a style instance
struct Style<T: UIView> {
    
    typealias Apply = (T) -> Void
    
    let apply: Apply
    
    func apply(to view: T) {
        apply(view)
    }
    
    func apply(to views: T...) {
        views.forEach {
            apply($0)
        }
    }
    
    static func composed(_ styles: Style<T>...) -> Style<T> {
        return Style { view in
            styles.forEach { style in
                style.apply(to: view)
            }
        }
    }
    
    func composed(with other: Style<T>) -> Style<T> {
        return Style { view in
            self.apply(to: view)
            other.apply(to: view)
        }
    }
    
    func composed(with apply: @escaping Apply) -> Style<T> {
        return composed(with: Style(apply: apply))
    }
}

let redBackgroundView: Style<UIView> = Style { view in
    view.backgroundColor = UIColor.red
}

let captionStyle: Style<UILabel> = Style { label in
    label.font = UIFont.systemFont(ofSize: 12)
}

let headingStyle: Style<UILabel>  = Style { label in
    label.textColor = UIColor.black
    label.font = UIFont.boldSystemFont(ofSize: 14)
}

struct AppStyle {
    
    private init() {
    }
    
    static let redBackgroundView: Style<UIView> = Style { view in
        view.backgroundColor = UIColor.red
    }
    
    static let caption: Style<UILabel> = Style { label in
        label.font = UIFont.systemFont(ofSize: 12)
    }
    
    static let title: Style<UILabel>  = Style { label in
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    static let heading: Style<UILabel> = Style { label in
        // Apply some nice styling so that the label is a heading
    }
}


class ViewController: UIViewController {
    
    let aView = UIView()
    let firstLabel = UILabel()
    let secondLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Option 1. Free functions
        apply(style: redBackgroundView, to: firstLabel)
        apply(style: headingStyle, to: secondLabel)
        
        // Option 2. Call method directly on a style instance
        headingStyle.apply(to: secondLabel)
        headingStyle.apply(to: firstLabel, secondLabel)
        
        // With option 3 (extensions)
        firstLabel.apply(style: redBackgroundView)
        firstLabel.apply(style: headingStyle)
        
        // Due to generics invariance, one can't call something like:
        //secondLabel.apply(style: redBackgroundView, captionStyle)
        // But one can achieve the same thing by using something like the above lines.
        // Also, you can "inherit" styles, but can compose them
        
        // By using composition
        let newNiceStyle = headingStyle.composed(with: captionStyle)
        
        let anotherNiceStyle = headingStyle.composed { label in
            label.adjustsFontSizeToFitWidth = true
        }.composed(with: newNiceStyle).composed { label in
            label.allowsDefaultTighteningForTruncation = false
        }
    }
    
}

// Any option can be enfored (constraint to use that option) by limiting visibility of functions

// Limtations: 
//      1. Invariance of generic types.
//          Can't call something like:        captionLabel.apply(styles: redBackgroundView, titleStyle)
//          So no inheritance, maybe composition instead? (One can work only with Style<T> types. For example, you couldn't combine Style<UIView> with Style<UILabel> in the same method call. It can be worked-around by chaining calls though.


// TODO:
// We need a Interface Builder solution
